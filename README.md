# by-the-numbers

## IFS 2023
- 13 people (10 students, 3 leaders).
- 20 days in-country.
- 46.98 GB of drone video (25 files), 63.55 Minutes of footage.
- 82.47 GB of drone imagery (6858 files), over 21,000,000m<sup>2</sup> surveyed (about 1/3 the area of Boston Township, Indiana).
- 921.36 GB of GroPro Fjord Videos (99 files), 2031.31 Minutes of footage.
- N songs on the Spotify playlist
