# Get total images and filesize from a directory
# Example: python3 get_vid_stats.py /Volumes/disk02/disk03-backup-jul10

import sys
import os
import cv2

ACCEPT_TYPES = [
    ".mp4",
    ".mov",
    ".avi"
]

TOTAL_VIDS = 0
TOTAL_GB = 0.0
TOTAL_MINS = 0.0

def with_opencv(filename):
    video = cv2.VideoCapture(filename)
    frame_count = video.get(cv2.CAP_PROP_FRAME_COUNT)
    fps = video.get(cv2.CAP_PROP_FPS)
    if fps != 0:
        return frame_count/fps
    return 0

if __name__ == '__main__':
    PATH = sys.argv[1]
    for subdir, dirs, files in os.walk(PATH):
        for file in files:
            filepath = subdir + "/" + file
            extension = os.path.splitext(filepath)[1]
            if extension.lower() in ACCEPT_TYPES and file[0] != ".":
                sizegb = os.stat(filepath).st_size / 1000 / 1000 / 1000
                duration = with_opencv(filepath)
                TOTAL_VIDS += 1
                TOTAL_GB += sizegb
                TOTAL_MINS += duration / 60

    print(str(TOTAL_VIDS) + " Video files")
    print(str(round(TOTAL_GB, 2)) + " GB")
    print(str(round(TOTAL_MINS, 2)) + " Minutes of footage")