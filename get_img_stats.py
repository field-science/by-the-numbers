# Get total images and filesize from a directory
# Example: python3 get_img_stats.py /Volumes/disk02/disk03-backup-jul10

import sys
import os

ACCEPT_TYPES = [
    ".dmg",
    ".jpg",
    ".jpeg",
    ".png"
]
TOTAL_IMGS = 0
TOTAL_GB = 0.0

if __name__ == '__main__':
    PATH = sys.argv[1]
    for subdir, dirs, files in os.walk(PATH):
        for file in files:
            filepath = subdir + "/" + file
            extension = os.path.splitext(filepath)[1]
            if extension.lower() in ACCEPT_TYPES and file[0] != ".":
                sizegb = os.stat(filepath).st_size / 1000 / 1000 / 1000
                TOTAL_IMGS += 1
                TOTAL_GB += sizegb
    print(str(TOTAL_IMGS) + " Images")
    print(str(round(TOTAL_GB, 2)) + " GB")
    